//PRUEBAS UNITARIAS

const sumar = require('../index');

const assert = require('assert'); //si ponemos solo el nombre de la libreria lo busca en node_modules

describe("Probar la suma de dos números.", () => {
    // Prueba para afirmar que cinco más cinco son 10
    it("Cinco más cinco es 10",()=>{ // Caso de prueba (it)
        assert.equal(10, sumar(5,5));
    });
    // Prueba para afirmar que cinco más cinco no son 55
    it("Cinco más cinco no son 55",()=>{
        assert.notEqual(55, sumar(5,5));
    })
});