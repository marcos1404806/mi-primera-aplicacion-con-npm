# Project Title

Práctica| Mi primera aplicación con NPM

## Getting Started/ Installing

Hacer pull o clonar el repositorio y en la terminal para las diferentes pruebas poner:
* node index.js
* npm run test
* ./node_modules/.bin.eslint index.js

### Prerequisites

* Tener distribucion de linux instalada.
* Tener node.js 

### And coding style tests

Estilo de escritura camelCase recomendado para java script.

## Built With

* Nodejs v12.22.9 --> Entorno en tiempo de ejecución multiplataforma
* JavaScript --> Lenguaje de programación.
* Eslint 
* Mocha
* log4js

## Contributing

No contribuyentes.

## Versioning

v1.0

## Authors

* Marcos Alfredo Aguilar Mata.

## License

Sin licencia.

## Acknowledgments

Código basado a lo visto en clase.

